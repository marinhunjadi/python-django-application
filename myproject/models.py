from django.db import models

class Patient(models.Model):
    GENDERS = (
        ('Male', 'Male'),
        ('Female', 'Female'),
    )    
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    gender = models.CharField(max_length=6, choices=GENDERS, null=True, default=None)
    date_of_surgery = models.DateField(null=True, default=None)
    icd10procedure = models.CharField(max_length=50, null=True, default=None)
    city = models.CharField(max_length=50, null=True, default=None)
    date_of_birth = models.DateField(null=True, default=None)
    icd10desc = models.CharField(max_length=255, null=True, default=None)
