from django.shortcuts import render
from datetime import datetime
#from django.http import HttpResponse
import logging
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from myproject.models import Patient

def home(request):
    return render(request, "myproject/home.html")

def upload_csv(request):
	data = {}
	if "GET" == request.method:
		return render(request, "myproject/home.html", data)
    # if not GET, then proceed
	try:
		csv_file = request.FILES["csv_file"]
		if not csv_file.name.endswith('.csv'):
			messages.error(request,'File is not CSV type')
			return HttpResponseRedirect(reverse("upload_csv"))
        #if file is too large, return
		if csv_file.multiple_chunks():
			messages.error(request,"Uploaded file is too big (%.2f MB)." % (csv_file.size/(1000*1000),))
			return HttpResponseRedirect(reverse("upload_csv"))

		file_data = csv_file.read().decode("utf-8")		

		lines = file_data.split("\n")
		#loop over the lines and save them in db. If error , store as string and then display
		for line in lines:						
			fields = line.split(",")                      
			try:
				Patient.objects.create(first_name=fields[1], last_name=fields[2], gender=fields[3], date_of_surgery=fields[4], icd10procedure=fields[5], city=fields[6], date_of_birth=(datetime.strptime(fields[7], '%m/%d/%Y').strftime('%Y-%m-%d') if fields[7].split('/', 1) != [''] else '0000-00-00'), icd10desc=fields[8])
												
			except Exception as e:
				logging.getLogger("error_logger").error(repr(e))					
				pass

	except Exception as e:
		logging.getLogger("error_logger").error("Unable to upload file. "+repr(e))
		messages.error(request,"Unable to upload file. "+repr(e))

	return HttpResponseRedirect(reverse("upload_csv"))

def hello_there(request, name):
    return render(
        request,
        'myproject/hello_there.html',
        {
            'name': name,
            'date': datetime.now()
        }
    )
