# Generated by Django 2.1.5 on 2019-01-06 15:55

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Patient',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('gender', models.CharField(choices=[('Male', 'Male'), ('Female', 'Female')], default=None, max_length=6, null=True)),
                ('date_of_surgery', models.DateField(default=None, null=True)),
                ('icd10procedure', models.CharField(default=None, max_length=50, null=True)),
                ('city', models.CharField(default=None, max_length=50, null=True)),
                ('date_of_birth', models.DateField(default=None, null=True)),
                ('icd10desc', models.CharField(default=None, max_length=255, null=True)),
            ],
        ),
    ]
