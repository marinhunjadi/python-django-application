from django.urls import path
from myproject import views

urlpatterns = [
    path("", views.home, name="home"),
    path("upload", views.upload_csv, name="upload_csv"),
    path("hello/<name>", views.hello_there, name="hello_there"),
]